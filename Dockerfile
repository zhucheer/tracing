FROM golang:1.13.10 AS builder

LABEL version="1.0" description="golang app" by="zhuqi"

WORKDIR /run/tracing

ENV GOPROXY https://goproxy.cn
ENV GOPRIVATE gitee.com
ENV GOSUMDB off




COPY . .
RUN go mod tidy

RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -a -o app-go .


FROM centos:7 AS final

RUN adduser apprun -u 10001

WORKDIR /data
COPY --from=builder /run/tracing/app-go /data/app/app-go


USER apprun

EXPOSE 8011
ENTRYPOINT ["/data/app/app-go"]