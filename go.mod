module gitee.com/zhucheer/tracing

go 1.13

require (
	github.com/SkyAPM/go2sky v1.2.0
	github.com/gin-gonic/gin v1.6.3
	github.com/labstack/gommon v0.3.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	skywalking.apache.org/repo/goapi v0.0.0-20210401062122-a049ca15c62d
)
