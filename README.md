# Gin框架接入skywalking服务


## 依赖管理

> 依赖包：https://gitee.com/zhucheer/tracing
> 通过 go moduls进行依赖管理

输入如下命令跳过相关校验

``` shell
go env -w GOPROXY=https://goproxy.cn,direct
go env -w GOPRIVATE=git.code.tencent.com,gitee.com
go env -w GOSUMDB=off
```

## 核心步骤

### 1.初始化启动

通过该方法填入 skywalking 服务的 grpc 地址和应用名称，应用名称可以是tsmkc_bss_service,tsmkc_mall等。

``` golang
sw.InitSkyWalking("0.0.0.0:11800", "token","[appName]")
```

### 2. 加载中间件

当请求进入后应用后会创建一个顶级 span

``` golang
router.Use(sw.EntrySpan(router))
```


### 3. http客户端加载exitSpan

因无法确定是否都是应用统一的http客户端请求包，我们需要在对外 http 请求时加上统一的header头

具体可以查看 `gin_example.go`
